<?php

  // <!-- // require '../vendor/autoload.php';
  // $settings = require_once __DIR__ . '/../app/settings.php';
  // $app = new \Slim\app($settings);
  // require_once __DIR__ . '/../app/dependencies.php';
  // require_once __DIR__ . '/../app/routes.php';
  // $app->run();

  // $app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
  //     $name = $args['name'];
  //     $response->getBody()->write("Hello, $name");
  //
  //     return $response;
  // });
  // $app->run();

  /////////////////////////// CODI CORRECTE ////////////////////////////////

  use \Psr\Http\Message\ServerRequestInterface as Request;
  use \Psr\Http\Message\ResponseInterface as Response;

  require '../vendor/autoload.php';

  $settings = require __DIR__ . '/../app/settings.php';
  $app = new \Slim\App($settings);

  //$app = new \Slim\App;
  require __DIR__. '/../app/routes.php';
  require __DIR__. '/../app/dependencies.php';
  $app->run();

  ////////////////////////////////////////////////////////////////////////// -->
?>
