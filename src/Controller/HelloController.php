<?php
namespace Pwbox\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 *
 */
class HelloController
{
  protected $container;

  function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  public function indexAction(Request $request, Response $response, array $arguments){
    $name = $request->getAttribute('name');
    //$name = $arguments['name'];
    return $this->container
      ->get('view')
      ->render(
          $response,
          'hello.twig',
          [
              'name' => $name
          ]
        );
  }

  public function __invoke(Request $request, Response $response, array $arguments){
    $name = $request->getAttribute('name');
    //$name = $arguments['name'];
    return $this->container
      ->get('view')
      ->render(
          $response,
          'hello.twig',
          [
              'name' => $name
          ]
        );
  }
}

 ?>
