<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class UserLoggerMiddleware
{
  public function __invoke(Request $request, Response $response, callable $nextl){
    if (!isset($_SESSION['user_id'])){
      return $response->withStatus(302)->withHeader('location', '/')
    }
    return $nextl($request, $response);
  }
}

 ?>
