<?php

$container = $app->getContainer();

// $container['view'] = function($container) {
//     $view = new \Slim\Views\Twig(__DIR__ . '/../src/view/templates', []);
//     $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
//     $view->addExtension(new \Slim\Views\TwigExtension($container['router'], $basePath));
//     return $view;
// };

//Register twig component
$container['view'] = function($container) {
    $view = new \Slim\Views\Twig('../src/view/templates', [
    ]);
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new \Slim\Views\TwigExtension($container['router'], $basePath));
    return $view;
};

//Register doctrine service
$container['doctrine'] = function($container){
  $config = new \Doctrine\DBAL\Configuration();
  $conn = \Doctrine\DBAL\DriveManager::getConnection(
    $container->get('settings')['database'],
    $config
  );
  return $conn;
};

//Register database implementation
$container['user_repository'] = function($container){
  $repository = new \Pwbox\model\implementations\DoctrineUserRepository(
    $container->get('doctrine')
  );
  return $repository;
};

//Register post user service
$container['post_user_service'] = function($container){
  $service = new \Pwbox\model\UseCase\PostUserUseCase(
    $container->get('user_repository')
  );
  return $service;
};
