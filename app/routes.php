<?php

  use \Psr\Http\Message\ServerRequestInterface as Request;
  use \Psr\Http\Message\ResponseInterface as Response;
  //
  // // $app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
  // //     $name = $args['name'];
  // //     return $this->view->render($response, 'hello.twig', ['name' => $name]);
  //
  //
  //     //$response->getBody()->write("Hello, $name");
  // $app->get(
  //   '/hello/{name}',
  //   'SlimApp\Controller\HelloController'
  // )->add('SlimApp\Controller\Middleware\ExampleMiddleware');
  //
  //     //return $response;
  // //});


  //$app = new \Slim\App;
  // require __DIR__. '/../app/routes.php';
  // $app->run();

  // $app->get('/hello/{name}', function(Request $request, Response $response) {
  //     $name = $request->getAttibute('name');
  //     $response->getBody()->write("Hello, $name");
  //     return $response;
  // });

//Slim diapo 1
  // $app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
  //     $name = $args['name'];
  //     $response->getBody()->write("Hello, $name");
  //
  //     return $response;
  // });

  //Slim diapo 2
  $app->get('/hello/{name}', function(Request $request, Response $response, array $args) {
      $name = $request->getAttribute('name');
      return $this->view->render($response, 'hello.twig', [
        'name' => $name
      ]);
      return $response;
  });

  //Slim diapo 3
  //$app->get('/hello/{name}', 'Pwbox\Controller\HelloController');
  //$app->get('/hello/{name}', 'Pwbox\Controller\HelloController:indexAction');

  //Slim diapo 3 middleware
  // $app->get('/hello/{name}', 'Pwbox\Controller\HelloController:indexAction'
  // )->add('Pwbox\Controller\Middleware\ExampleMiddleware');
